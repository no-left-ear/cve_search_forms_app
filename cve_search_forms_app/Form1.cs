﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace cve_search_forms_app
{
    public partial class Form1 : Form
    {
        static string baseUri = "https://cve.circl.lu/api/";
        public Form1()
        {
            InitializeComponent();
        }

        // Closes the form
        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // If inputTextBox is empty, makes a call to /api/last
        // If inputTextBox is not empty, tries to make a call to /api/cve/<cveID>
        //   where <cveID> is the value in inputTextBox. 
        private void searchButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(inputTextBox.Text))
            {
                getLastThirtyCVES();
            }
            else
            {
                getSpecificCveID(inputTextBox.Text);
            }
        }

        // Clears the multiline outputBox,
        //   Refills the placeholder text,
        //   and disables the save button,
        private void clearOutputBox_Click(object sender, EventArgs e)
        {
            outputBox.Clear();
            outputBox.AppendText("Enter a CVE ID to search for..." + Environment.NewLine + "Leave blank to get last 30 CVEs");
            saveButton.Enabled = false;
        }

        // Makes an HTTP API call and tries to retrieve information of a specific CVE ID
        // If the Status Code is not 200-299, or if the returned content is the string "null"
        //   replaces the placeholder text with an error message.
        // If all is clear, shows the content in the multiline textbox in pretty-printed JSON
        //   Also enables the save button
        private async void getSpecificCveID(string cveID)
        {
            outputBox.Clear();
            // Temporarily disables search and clear buttons until the process is completed.
            searchButton.Enabled = false;
            clearOutputBox.Enabled = false;
            var inputString = cveID;
            using (var apiClient = new HttpClient())
            {
                apiClient.BaseAddress = new Uri(baseUri);
                apiClient.DefaultRequestHeaders.Accept.Clear();
                apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                outputBox.AppendText("Searching for: cve/" + inputString + Environment.NewLine);
                HttpResponseMessage resp = await apiClient.GetAsync("cve/" + inputString);
                if (!resp.IsSuccessStatusCode || resp.Content.ReadAsStringAsync().Result == "null")
                {
                    outputBox.AppendText("Could not find: cve/" + inputString + Environment.NewLine);
                    outputBox.AppendText("Please check spelling and search again." + Environment.NewLine);
                }
                else
                {
                    outputBox.Clear();
                    outputBox.AppendText(JValue.Parse(resp.Content.ReadAsStringAsync().Result).ToString(Formatting.Indented));
                    saveButton.Enabled = true;
                }
                searchButton.Enabled = true;
                clearOutputBox.Enabled = true;
            }
        }

        // Makes an HTTP API call and tries to retrieve information last 30 CVE's
        // If the Status Code is not 200-299, or if the returned content is the string "null"
        //   replaces the placeholder text with an error message.
        // If all is clear, shows the content in the multiline textbox in pretty-printed JSON
        //   Also enables the save button
        private async void getLastThirtyCVES()
        {
            outputBox.Clear();
            // Temporarily disables search and clear buttons until the process is completed.
            searchButton.Enabled = false;
            clearOutputBox.Enabled = false;
            using (var apiClient = new HttpClient())
            {
                apiClient.BaseAddress = new Uri(baseUri);
                apiClient.DefaultRequestHeaders.Accept.Clear();
                apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                outputBox.AppendText("Searching for the last 30 CVE's, including CAPEC, CWE and CPE expansions." + Environment.NewLine);
                HttpResponseMessage resp = await apiClient.GetAsync("last");
                if (!resp.IsSuccessStatusCode)
                {
                    outputBox.AppendText("There was a problem retrieving the last 30 CVE's." + Environment.NewLine);
                }
                else
                {
                    outputBox.Clear();
                    outputBox.AppendText(JValue.Parse(resp.Content.ReadAsStringAsync().Result).ToString(Formatting.Indented));
                    saveButton.Enabled = true;
                }
                searchButton.Enabled = true;
                clearOutputBox.Enabled = true;
            }
        }

        // Creates a saveFileDialog to choose where to save
        //   the json output file.
        // Re-enables the clear and search buttons when completed.
        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveOutput = new SaveFileDialog();
            saveOutput.InitialDirectory = "@C:/";
            saveOutput.RestoreDirectory = true;
            saveOutput.FileName = "*.json";
            saveOutput.DefaultExt = "json";
            saveOutput.Filter = "Json files(*.json)| *.json";

            if (saveOutput.ShowDialog() == DialogResult.OK)
            {
                StreamWriter outputStream = new StreamWriter(saveOutput.OpenFile());
                outputStream.Write(outputBox.Text);
                outputStream.Close();
                outputBox.Clear();
                outputBox.AppendText("File saved at: " + saveOutput.FileName);
                saveButton.Enabled = false;
                clearOutputBox.Enabled = true;
                searchButton.Enabled = true;
            }
        }
    }
}
