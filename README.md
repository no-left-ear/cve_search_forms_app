# Project Title

CVE API Exercise

## Description

The Visual Studio 2019 source code for the Windows Forms version of the CVE API Exercise.

## Getting Started

https://marketplace.visualstudio.com/items?itemName=MistyK.VisualStudioBitbucketExtension

 -or-
 
Clone like normal. Open the .sln file in Visual Studio.

### Dependencies

* Windows OS
* Visual Studio


### Executing program

To run in VS, simply hit F5, or navigate to Debug-->Start Debugging on the VS toolbar.

The default value in the Search Box is a valid CVE ID. Click *Search* to look for it.

If the Search Box is left blank, a different API call to retrieve the last 30 entries will occur instead.

If an invalid value is put in the box, the output box will display errors.

Click *Clear* at any time that it is active to clear out the output box and start over.

Once a valid search is completed, *Save To...* will enable and allow
the user to use saveFileDialog to save the text in the output box to a file.


## Authors

Joshua Hurst


## Acknowledgments

* https://sites.google.com/site/wcfpandu/web-api/calling-a-web-api-from-c-and-calling-a-web-api-from-view
* https://youtu.be/aWePkE2ReGw
* https://docs.microsoft.com/en-us/aspnet/web-api/overview/advanced/calling-a-web-api-from-a-net-client
* https://youtu.be/b_8EfZkyMZg

### Additional Notes

If looking for just an executable, check out cve_search_app_form_published instead. 